# The Photo App
### Demo [https://www.loom.com/share/7aa685bbf6d348c8957a7c5f1187c06c](https://www.loom.com/share/7aa685bbf6d348c8957a7c5f1187c06c)

How to run
`` yarn & yarn start``

Technologies. 
* Redux
* Thunk
* Service Layer architecture with service manager
* Custom GraphQL consumer 
