import React, {useEffect, useState} from "react";
import {Card, Checkbox } from 'antd';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';



interface Props {
    id: number,
    url: string,
    isSelectable ?: boolean | false,
    onSelected ?: any
    onDeselected ?: any
    disableIfNotSelected ?: boolean | false,
}


function triggerCallbacks(isSelected: boolean, props: Props) {
    const photo: any = {id: props.id, url: props.url};

    if (!props.disableIfNotSelected && isSelected) {
     props.onSelected(photo);
     return;
    }

    props.onDeselected(photo)
}

const PhotoCard = (props: Props) => {

    const [ isSelected, setIsSelected ] = useState(false);
    const [ isComponentLoaded, setIsComponentLoaded ] = useState(false);

    useEffect(() => {
        if (isComponentLoaded) {
            triggerCallbacks(isSelected, props);
        }
    }, [isSelected]);

    useEffect(() => {
        setIsComponentLoaded(true);
    }, []);

    return <Card
        hoverable
        onClick={() => {
            if (props.disableIfNotSelected && !isSelected) {
                return;
            }
            setIsSelected(!isSelected)
        }}
        bordered={true}
        extra={props.isSelectable && <Checkbox checked={isSelected}/>}
    >
        <LazyLoadImage
            height={"200"}
            effect="blur"
            src={props.url}
            width={"100%"} />

    </Card>
};

export default PhotoCard;
