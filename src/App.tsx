import React from 'react';
import './App.css';
import { Layout } from 'antd';
import {
    BrowserRouter as Router,
    Switch,
    Route, Link,
} from "react-router-dom";

import PhotoLibraryContainer from "./containers/photo-library.container";
import PhotoGridContainer from "./containers/photo-grid.container";

const { Header } = Layout;

const App = () => {

    return (
        <Router>
            <Layout className="layout" style={{height:"100vh"}}>
                <Header>
                    <div className="logo" > <Link to={"/"}> <h1 style={{ color: "white"}}>The Photo App</h1></Link></div>
                </Header>
                    <Switch>
                        <Route exact path="/">
                            <PhotoGridContainer/>
                        </Route>
                        <Route path="/library">
                            <PhotoLibraryContainer/>
                        </Route>
                    </Switch>
            </Layout>
        </Router>

    );
};

export default App;
