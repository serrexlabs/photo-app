import Action from "./type";
import {ServiceManagerInterface} from "../../core/service-manager";
import PhotoGridService from "../../services/photo-grid-service";

const PREFIX = "@@photoGrid/";
export const ON_PHOTO_SELECTED = `${PREFIX}ON_PHOTO_SELECTED`;
export const ON_PHOTO_DESELECTED = `${PREFIX}ON_PHOTO_DESELECTED`;
export const CREATE_PHOTOS_GRID_ERROR = `${PREFIX}CREATE_PHOTOS_GRID_ERROR`;
export const CREATE_PHOTOS_GRID_REQUEST = `${PREFIX}CREATE_PHOTOS_GRID_REQUEST`;
export const CREATE_PHOTOS_GRID_SUCCESS = `${PREFIX}CREATE_PHOTOS_GRID_SUCCESS`;

const photoGridRequest = (): Action => {
    return {
        type: CREATE_PHOTOS_GRID_REQUEST
    };
};

const photoGridSuccess = (photoGrid: any): Action=> {
    return {
        type: CREATE_PHOTOS_GRID_SUCCESS,
        payload: {
            photoGrid
        }
    };
};


export const photoSelected = (photo: any): Action => {
    return {
        type: ON_PHOTO_SELECTED,
        payload: {
            photo
        }
    };
};

export const photoDeselected = (photo: any): Action => {
    return {
        type: ON_PHOTO_DESELECTED,
        payload: {
            photo
        }
    };
};

export const createPhotoGridAsync = () => {
    return (dispatch: any, getState: any, serviceManager: ServiceManagerInterface) => {
        dispatch(photoGridRequest());
        const photoGridService: PhotoGridService = serviceManager.get("PhotoService");
        const state = getState();

        photoGridService.createPhotoGrid(state.photoGrid.selectedPhotos)
            .then(photoGrid => dispatch(photoGridSuccess(photoGrid)));
    };
};

export const getPhotoGridAsync = () => {
    return (dispatch: any, getState: any, serviceManager: ServiceManagerInterface) => {
        dispatch(photoGridRequest());
        const photoGridService: PhotoGridService = serviceManager.get("PhotoService");

        photoGridService.getPhotoGrid()
            .then(photoGrid => dispatch(photoGridSuccess(photoGrid)));
    };
};


