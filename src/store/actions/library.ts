import Action from "./type";
import {ServiceManagerInterface} from "../../core/service-manager";
import LibraryService from "../../services/library-service";
import config from "../../config/config";

const PREFIX = "@@library/";
export const FETCH_LIBRARY_PHOTOS_ERROR = `${PREFIX}FETCH_LIBRARY_PHOTOS_ERROR`;
export const FETCH_LIBRARY_PHOTOS_REQUEST = `${PREFIX}FETCH_LIBRARY_PHOTOS_REQUEST`;
export const FETCH_LIBRARY_PHOTOS_SUCCESS = `${PREFIX}FETCH_LIBRARY_PHOTOS_SUCCESS`;

const fetchLibraryPhotoRequest = (): Action => {
    return {
        type: FETCH_LIBRARY_PHOTOS_REQUEST
    };
};

const fetchLibraryPhotoError = (err: any): Action => {
    return {
        type: FETCH_LIBRARY_PHOTOS_ERROR,
        payload: {
            err
        }
    };
};

const fetchLibraryPhotoSuccess = (photos: any): Action=> {
    return {
        type: FETCH_LIBRARY_PHOTOS_SUCCESS,
        payload: {
            photos
        }
    };
};
export const fetchLibraryPhotosAsync = () => {
    return (dispatch: any, getState: any, serviceManager: ServiceManagerInterface) => {

        dispatch(fetchLibraryPhotoRequest());


         let libraryService: LibraryService = serviceManager.get('LibraryService');

        /**
         * NOTE: Assuming the user is already logon and the following ID is his user id
         */
        return libraryService
            .getPhotos("CHhASmTpKjaHyAsSaauThRqMMjWanYkQ")
            .then((response: any) => {
                const photos = response.entries.map((photo: any): any => {
                    /**
                     * NOTE: the give file size is higher for grid view, since flicker provided a filestack function, here building resize function url.
                     */
                    return {
                        url: `${config.FILESTACK_RESIZE_URL}${photo.picture.split("/").pop()}`,
                        id: photo.id
                    }
                } );
                dispatch(fetchLibraryPhotoSuccess(photos));
            })
            .catch((err: any) => {
                dispatch(fetchLibraryPhotoError(err));
            });
    };
};
