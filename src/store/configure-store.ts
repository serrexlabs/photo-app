import { createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';
import { composeWithDevTools } from "redux-devtools-extension";
import rootReducer from "./reducers";
import {register as registerServices} from '../config/services';

export default () => {
    const  serviceManager = registerServices();

    const middlewareEnhancer = applyMiddleware(thunk.withExtraArgument(serviceManager));
    let composedEnhancers = compose(middlewareEnhancer);

    if (process.env.NODE_ENV === "development") {
        composedEnhancers = composeWithDevTools(middlewareEnhancer);
    }

    return createStore(rootReducer, {}, composedEnhancers);
};
