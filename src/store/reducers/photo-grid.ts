import Action from "../actions/type";
import * as PhotoGridActions from "../actions/photo-grid";

export interface PhotoGridState {
    selectedPhotos : any[] | [];
    grid ?: {id: string, photos: any[]} | null;
    isLoading ?: boolean;
}

const initialState: PhotoGridState = {
    selectedPhotos: [],
    isLoading: false,
    grid: null
};

function onPhotoSelected(state: PhotoGridState, photo: any) {
    return {
        ...state,
        selectedPhotos: [...state.selectedPhotos , photo]
    };
}

function onPhotoDeselected(state: PhotoGridState, photo: any) {

    if (state.selectedPhotos.length <= 0) {
        return {
            ...state,
            selectedPhotos: [photo],
        };
    }

    return {
        ...state,
        selectedPhotos: state.selectedPhotos.filter((item) => item.id !== photo.id),
    };
}

function reducePhotoGrid(state: PhotoGridState, photoGrid: any) {
    return {
        ...state,
        isLoading: false,
        grid: photoGrid
    };
}

export default (state = initialState, action: Action) => {
    switch (action.type) {
        case PhotoGridActions.ON_PHOTO_SELECTED:
            return onPhotoSelected(state, action.payload.photo);
        case PhotoGridActions.ON_PHOTO_DESELECTED:
            return onPhotoDeselected(state, action.payload.photo);
        case PhotoGridActions.CREATE_PHOTOS_GRID_SUCCESS:
            return reducePhotoGrid(state, action.payload.photoGrid);
        case PhotoGridActions.CREATE_PHOTOS_GRID_REQUEST:
            return {
                ...state,
                isLoading: true
            };
        default:
            return state;
    }
};
