import Action from "../actions/type";
import * as LibraryActions from "../actions/library";

export interface LibraryState {
    photos ?:any[] | [];
    isLoading ?: boolean;
}

const initialState: LibraryState = {
    photos: [],
    isLoading: false
};

function onLibraryPhotoLoading(state: LibraryState) {
    return {
        ...state,
        isLoading: true,
    };
}

function onLibraryPhotoLoaded(state: LibraryState, photos: any) {
    return {
        ...state,
        photos,
    };
}

export default (state = initialState, action: Action) => {
    switch (action.type) {
        case LibraryActions.FETCH_LIBRARY_PHOTOS_REQUEST:
            return onLibraryPhotoLoading(state);
        case LibraryActions.FETCH_LIBRARY_PHOTOS_SUCCESS:
            return onLibraryPhotoLoaded(state, action.payload.photos);
        default:
            return state;
    }
};
