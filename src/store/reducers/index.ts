import { combineReducers } from "redux";
import library, {LibraryState} from "./library";
import Action from "../actions/type";
import photoGrid, {PhotoGridState} from "./photo-grid";

export interface StoreInterface {
    library: LibraryState,
    photoGrid: PhotoGridState
}

const appReducer = combineReducers({
    library,
    photoGrid,
});

export default (state: any, action: Action) => {
    return appReducer(state, action);
};
