import React, {useState} from "react";
import {List, Card, Steps, Layout, Affix, Button} from 'antd';
import PhotoCard from "../components/PhotoCard";
import {useDispatch} from "react-redux";
import {createPhotoGridAsync, photoDeselected, photoSelected} from "../store/actions/photo-grid";

const { Content } = Layout;

const { Step } = Steps;

interface Props {
    selectableItemCount: number,
    photos: any,
}

const ActionBar = (props: {selectableItemCount: number, current: number}) => {
    const dispatch = useDispatch();

    // Creating progress steps
    const steps: any = [];
    for (let i = 0; i < props.selectableItemCount; i++) {
        steps.push({ title: (i+1).toString()});
    }
    return (
        <Affix offsetTop={10}>
            <Card>
                <Steps current={props.current} progressDot>
                    {steps.map((item: any) => (
                        <Step key={item.title} title={item.title} />
                    ))}
                </Steps>
                { props.current >= (props.selectableItemCount - 1) && <Button type={"primary"} style={{ float: "right"}} onClick={() => dispatch(createPhotoGridAsync())}> Save the photo book  </Button>}
            </Card>
        </Affix>
    );
}

const onSelect = (props: Props, photo: any, current: number, setCurrent: any, dispatch: any) => {
    if (props.selectableItemCount <= current) {
        return;
    }
    setCurrent( current + 1);
    dispatch(photoSelected(photo));
};

const onDeselect = (props: Props, photo: any, current: number, setCurrent: any, dispatch: any) => {
    if (current <= 0) {
        return;
    }
    setCurrent( current - 1);
    dispatch(photoDeselected(photo));

};

const PhotoLibrary = (props: Props) => {
    const [ current, setCurrent ] = useState(-1);
    const dispatch = useDispatch();

    return <Content style={{ padding: '20px 50px' }}>
        <div style={{ background: '#fff', padding: 24, height: "100%"}}>
            <ActionBar current={current} selectableItemCount={props.selectableItemCount}/>
            <List
                grid={{ gutter: 16, column: 4 }}
                dataSource={props.photos}
                renderItem={(photo: any) => (
                    <List.Item>
                      <PhotoCard
                          isSelectable={true}
                          onDeselected={(photo: any) =>  onDeselect(props, photo, current, setCurrent, dispatch)}
                          onSelected={ (photo: any) => onSelect(props, photo, current, setCurrent, dispatch) }
                          url={photo.url}
                          id={photo.id}
                          disableIfNotSelected={ current >= (props.selectableItemCount - 1) }
                      />
                    </List.Item>
                )}
            />
        </div>
    </Content>
};

export default PhotoLibrary;
