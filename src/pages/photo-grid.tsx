import React from "react";
import {Button, Empty, Layout, List} from "antd";
import {Link} from "react-router-dom";
import PhotoCard from "../components/PhotoCard";

const { Content } = Layout;

const GotoLibraryButton = () => {
  return <Link to={"/library"}>
       <Button type={"primary"}> Create Photo Book</Button>
  </Link>
};

const PhotoGrid = (props: any) => {
    return <Content style={{ padding: '20px 50px' }}>
        <div style={{ background: '#fff', padding: 24, height: "100%"}}>
            { !props.photos && <Empty description={"It seems that you haven't created any photo book yet!"} children={<GotoLibraryButton />} /> }

            <List
                grid={{ gutter: 16, column: 4 }}
                dataSource={props.photos}
                renderItem={(photo: any) => (
                    <List.Item>
                        <PhotoCard
                            isSelectable={false}
                            onDeselected={() => {}}
                            onSelected={() => {}}
                            url={photo.url}
                            id={photo.id}
                        />
                    </List.Item>
                )}
            />
        </div>
    </Content>
};

export default PhotoGrid;
