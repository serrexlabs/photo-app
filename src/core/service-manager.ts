export interface ServiceManagerInterface {
    register(serviceName: string, registerCallback: any): void;
    get(serviceName: string): any;
}

export class ServiceManager implements ServiceManagerInterface{
    private factories: any = {};

    private instances: any = {};

    private callStack: any = [];

    constructor() {
        Object.defineProperty(this, 'instances', {
            writable: false,
            enumerable: true,
            value: this.instances
        });

        Object.defineProperty(this, 'factories', {
            writable: false,
            enumerable: true,
            value: this.factories
        });
    }

    register(name: string, factory: any) {
        this.factories[name] = factory;
        this.instances[name] = undefined;
    }


    private instantiate(name: string) {
        if (this.factories[name] === undefined) {
            throw new Error(`Unknown Service of ${name}`);
        }

        if (typeof this.factories[name] !== 'function') {
            return this.factories[name];
        }

        if (!this.isSafeToInvoke(name)) {
            throw new Error(`Recursive Dependency of ${name}`);
        }

        this.callStack.push(name);
        let instance = this.factories[name](this);

        if (instance === undefined) {
            throw new Error(`Service Creation Error of ${name}`);
        }

        this.instances[name] = instance;

        this.callStack = [];

        return this.instances[name];
    }


    private isSafeToInvoke(name: string) {
        return this.callStack.indexOf(name) === -1;
    }

    get(name: string) {
        return this.instances[name] || this.instantiate(name);
    }

}
