
export default class ApiService {
    private baseUrl: string;

    constructor(baseUrl: string) {
        this.baseUrl = this.formatApiEndpoint(baseUrl);
    }

    private formatApiEndpoint(baseUrl: string) {
        return baseUrl.replace(/\/$/, '');
    }

   public get(endpoint: string) {
        return this.fetch('GET', endpoint);
   }

    public post(endpoint: string, body: any | {}) {
        return this.fetch('POST', endpoint, body);
    }

   private fetch(method: string, endpoint: string, body = {}) {

        const options: { headers: any, method: string, body ?: any} = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            method,
        };

        if (Object.keys(body).length > 0) {
            options.body = typeof body === 'object' ? JSON.stringify(body) : body;
        }

        return fetch(`${this.baseUrl}${endpoint}`, options)
            .catch((error) => {
                throw new Error(error);
            })
            .then((response) => {

                if (response.status >= 500) {
                    throw new Error('Internal Server Error');
                }

                if (response.status !== 200 && response.ok === true) {
                    return response;
                }

                return response.json();
            });
    }

}
