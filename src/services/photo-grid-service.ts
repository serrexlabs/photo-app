import GraphqlService from "./graphql-service";

export default class PhotoGridService {

    private graphqlService: GraphqlService;

    constructor(graphqlService: GraphqlService) {
        this.graphqlService = graphqlService;
    }

    public getPhotoGrid() {
        return this.graphqlService.execute(`
             {
                  photoGrid {
                    id
                    photos {
                      id
                      url
                    }
                  }
            }`).then(response => {
            return response.data.photoGrid[0]
        });
    }

    public createPhotoGrid(photos: any) {
        const query = `
       mutation createPhotoGird($photos: [GraphQLPhotoInput]) {
          photoGrid {
            add(photos: $photos) {
              id
              photos {
                id
                url
              }
            }
          }
        }`;

        return this.graphqlService.execute(query, { photos })
        .then(response => {
           return response.data.photoGrid.add
        });
    }
}
