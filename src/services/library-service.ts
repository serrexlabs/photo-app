import ApiService from "./api-service";

export default class LibraryService {

    private apiEndpoint = '/collection';
    private api: ApiService;

    constructor(apiService: ApiService) {
        this.api = apiService;
    }

    // This context should limited to user, but this app build on assumption where the user authorization does not consider
    getPhotos(userID: any) {
        return this.api.get(`${this.apiEndpoint}/${userID}.json`).then((response) => {
            console.log(response);
            return response;
        });
    }
}
