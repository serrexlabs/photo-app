import ApiService from "./api-service";

export default class GraphqlService {

    private apiEndpoint = '/graphql';
    private api: ApiService;

    constructor(apiService: ApiService) {
        this.api = apiService;
    }

    public execute(query: string, variables ?: object | null) {

        let body: {} = {
            query,
        };

        if (variables !== null) {
            body = {
                ...body,
                variables,
            };
        }

        return this.api.post(`${this.apiEndpoint}`, JSON.stringify(body)).then((response) => {
            return response;
        });
    }
}
