import React, {useEffect} from "react";
import PhotoGrid from "../pages/photo-grid";
import {useDispatch, useSelector} from "react-redux";
import {StoreInterface} from "../store/reducers";
import {getPhotoGridAsync} from "../store/actions/photo-grid";

const PhotoGridContainer = (): any =>  {
    const dispatch = useDispatch();
    const photos = useSelector((state: StoreInterface) => state.photoGrid.grid?.photos);


    useEffect(() => {
        dispatch(getPhotoGridAsync())
    }, []);

    return <PhotoGrid photos={photos} />
};

export default PhotoGridContainer;
