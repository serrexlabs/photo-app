import React, {useEffect} from "react";
import PhotoLibrary from "../pages/photo-library";
import {fetchLibraryPhotosAsync} from "../store/actions/library";
import {useDispatch, useSelector} from "react-redux";
import {StoreInterface} from "../store/reducers";

const PhotoLibraryContainer = (): any =>  {
    const dispatch = useDispatch();
    const photos = useSelector((state: StoreInterface) => state.library.photos);


    useEffect(() => {
        dispatch(fetchLibraryPhotosAsync())
    }, [photos]);

    {/*  selectableItemCount: its better to get this value form config or any other dynamic source */}
    return <PhotoLibrary selectableItemCount={9} photos={photos}/>
};

export default PhotoLibraryContainer;
