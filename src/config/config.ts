/**
 * If there any sensitive data, I would move them into a environment variables
 * ex: process.env.REACT_APP_API_KEY
 * */

const dev = {
    API_URL: "http://localhost:2010/",
    /**
     * NOTE: the given endpoint doesn't allow the cors, had to use a cors proxy to avoid unnecessary changes
     */
    LIBRARY_URL: "https://cors-anywhere.herokuapp.com/https://dev-pb-apps.s3-eu-west-1.amazonaws.com/",
    /**
     * NOTE: using filestack endpoint to get smaller image of flicker files
     */
    FILESTACK_RESIZE_URL: "https://cdn.filestackcontent.com/resize=h:200,w:200,f:%22crop%22/",
};

const prod = {
    API_URL: "http://localhost:2010",
    /**
     * NOTE: the given endpoint doesn't allow the cors, had to use a cors proxy to avoid unnecessary changes
     */
    LIBRARY_URL: "https://cors-anywhere.herokuapp.com/https://dev-pb-apps.s3-eu-west-1.amazonaws.com/",
    /**
     * NOTE: using filestack endpoint to get smaller image of flicker files
     */
    FILESTACK_RESIZE_URL: "https://cdn.filestackcontent.com/resize=h:200,w:200,f:%22crop%22/",
};

const config = process.env.NODE_ENV === 'production'
    ? prod
    : dev;


export default {
    ...config
};
