import {ServiceManager, ServiceManagerInterface} from '../core/service-manager';
import LibraryService from "../services/library-service";
import ApiService from "../services/api-service";
import config  from './config';
import GraphqlService from "../services/graphql-service";
import PhotoGridService from "../services/photo-grid-service";



export const register = (serviceManager: ServiceManagerInterface = new ServiceManager()) => {

    serviceManager.register('LibraryService', (serviceManager: ServiceManagerInterface) => {
        return new LibraryService(new ApiService(config.LIBRARY_URL));
    });

    serviceManager.register('GraphqlService', (serviceManager: ServiceManagerInterface) => {
        return new GraphqlService(new ApiService(config.API_URL));
    });

    serviceManager.register('PhotoService', (serviceManager: ServiceManagerInterface) => {
        const graphQLService: GraphqlService = serviceManager.get("GraphqlService");
        return new PhotoGridService(graphQLService);
    });

    return serviceManager;
};
